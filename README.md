# �vesq_'s KZ remastered quakesounds

---

## Installation


---
* For KZTimer 
* Move the `sound` folder to `/csgo/` and overwrite everything
* For GOKZ
* Move the __files__ inside the `sound` folder to `/csgo/sound/gokz` and overwrite everything
---
* Start your CSGO
* Now your Quake sounds should be changed
* Jump some world records!

---

## Feedback Sound Installation

* Do the above
* Pick your favourite file from `sound/feedback/`
* Copy the path of the file from `sound`
* Use the command "playvol `filepath` `volume`"
* Bind that to your desired key, see the example below

### Example
* alias +movefwd "+forward;playvol feedback/2013blackwidow.mp3 1"
* alias -movefwd "-forward"
* bind W +movefwd

---

## Changelog

### v1.0 
* "dark" sounds for rampage, dominating and unstoppable
* "female" sound for wickedsick_new

### v1.1 
* holyshit_new.mp3 sound changed 

### v1.2 
* godlike.mp3 sound changed to chuckles sound

### v1.3
* added keypress feedback sound folder and instructions

### v1.4.1
* changed perfect, impressive, ownage, holyshit and wickedsick sounds

---